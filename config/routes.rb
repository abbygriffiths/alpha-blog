Rails.application.routes.draw do
  resources :articles
  resources :users, except: [:new]
  resources :categories, except: [:destroy]

  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy'
  
  get 'signup', to: 'users#new'

  get 'toggle_admin', to: 'users#toggle_admin'
  
  root 'pages#home'
  get 'about', to: 'pages#about'
end
